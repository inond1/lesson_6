#include <iostream>
#define ILLEGAL_NUMBER 8200

#define ERROR_NUMBER -1


int add(int a, int b) {
	if (a == ILLEGAL_NUMBER || b == ILLEGAL_NUMBER || a + b == ILLEGAL_NUMBER) {
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearence in 1 year.\n";
		return ERROR_NUMBER; }
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
		if (sum == ERROR_NUMBER) { return sum; }
		else if (sum == ILLEGAL_NUMBER) { 
			std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearence in 1 year.\n";
			return ERROR_NUMBER; }
	}
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		if (exponent == ERROR_NUMBER) { return exponent; }
		else if (exponent == ILLEGAL_NUMBER) {
			std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearence in 1 year.\n";
			return ERROR_NUMBER; }
	}
	return exponent;
}

int main(void) {
	std::cout << multiply(1025,8) << std::endl;
}