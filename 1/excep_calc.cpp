#include <iostream>

#define ILLEGAL_NUMBER 8200

int add(int a, int b) {
	if (a == ILLEGAL_NUMBER || b == ILLEGAL_NUMBER || a + b == ILLEGAL_NUMBER) {
		throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearence in 1 year.\n"); }
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
		if (sum == ILLEGAL_NUMBER) {
			throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearence in 1 year.\n");
		}
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		if (exponent == ILLEGAL_NUMBER) {
			throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearence in 1 year.\n");
		}
	};
	return exponent;
}

int main(void) {
	try
	{
		add(8199, 1);
	}
	catch (const std::string e)
	{
		std::cerr << "[ERROR] " << e;
	}
	try
	{
		multiply(1025, 8);
	}
	catch (const std::string e)
	{
		std::cerr << "[ERROR] " << e;
	}
	try
	{
		pow(8200, 1);
	}
	catch (const std::string e)
	{
		std::cerr << "[ERROR] " << e;
	}
}