#pragma once
#include "mathutils.h"

double mathUtils::CalPentagonArea(const double side)
{
	return PARAM_PEN * side * side;
}

double mathUtils::CalHexagonArea(double side)
{
	return PARAM_HEX * side * side;
}
