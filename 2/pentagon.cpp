#include "pentagon.h"

Pentagon::Pentagon(std::string nam, std::string col, double side):Shape(nam, col)
{
	if (side < 0)
	{
		shapeException ex;
		throw ex;
	}
	member = side;
}

double Pentagon::getMember() const
{
	return member;
}

void Pentagon::setMember(const double side)
{
	member = side;
}

double Pentagon::calArea()
{
	return mathUtils::CalPentagonArea(member);
}

void Pentagon::draw()
{
	std::cout << "name is " << getName() << "\n color is " << getColor() << "\n member is " << getMember() << "\n Area is " << calArea()<<std::endl;
}
