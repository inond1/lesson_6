#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "hexagon.h"
#include "pentagon.h"
#include <string>
#include "inputexception.h"
#include "shapeException.h"
#define MAX_BUFFER 256
void checkInput();

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, member = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Hexagon hex(col, nam, member);
	Pentagon pen(col, nam, member);


	Shape* ptrcirc = &circ;
	Shape* ptrquad = &quad;
	Shape* ptrrec = &rec;
	Shape* ptrpara = &para;
	Shape* ptrhex = &hex;
	Shape* ptrpen = &pen;


	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', hexagon = 'h', pentagon = 'P'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, P = pentagon" << std::endl;
		std::cin >> shapetype;
		}
		try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				checkInput();
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				checkInput();
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				checkInput();
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				checkInput();
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
			case 'h':
				std::cout << "enter name, color, member" << std::endl;
				std::cin >> nam, col, member;
				checkInput();
				hex.setName(nam);
				hex.setColor(col);
				hex.setMember(member);
				ptrhex->draw();
			case 'P':
				std::cout << "enter name, color, member" << std::endl;
				std::cin >> nam, col, member;
				checkInput();
				pen.setName(nam);
				pen.setColor(col);
				pen.setMember(member);
				ptrpen->draw();
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (inputException & e)
		{
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		catch (std::exception& e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



	system("pause");
	return 0;

}

void checkInput()
{
	if (std::cin.fail())
	{
		inputException ex;
		std::cin.clear();
		throw ex;
	}
}
