#pragma once

#define PARAM_PEN 1.72048
#define PARAM_HEX 2.59808

class mathUtils
{
public:
	static double CalPentagonArea(const double side);
	
	static double CalHexagonArea(double side);
};