#include "hexagon.h"

Hexagon::Hexagon(std::string nam, std::string col, double side):Shape(nam, col)
{
	if (side < 0)
	{
		shapeException ex;
		throw ex;
	}
	member = side;
}

double Hexagon::getMember()const 
{
	return member;
}

void Hexagon::setMember(const double side)
{
	member = side;
}

double Hexagon::calArea()
{
	return mathUtils::CalHexagonArea(member);
}

void Hexagon::draw()
{
	std::cout << "name is " << getName() << "\n color is " << getColor() << "\n member is " << getMember() << "\n Area is " << calArea()<<std::endl;
}
