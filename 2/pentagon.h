#pragma once
#include "shape.h"
#include "mathutils.h"
#include "shapeexception.h"

class Pentagon : public Shape
{
private:
	double member;
public:
	Pentagon(std::string, std::string, double);
	double getMember() const;
	void setMember(const double side);
	virtual double calArea();
	virtual void draw();
};