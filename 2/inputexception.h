#pragma once
#include <exception>

class inputException : public std::exception
{
	virtual const char* what() const
	{
		return "This is an input exception!";
	}
};